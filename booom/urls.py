from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'booom.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^get-towns/', 'myapp.views.getTowns'),
    url(r'^get-categories/', 'myapp.views.getCategories'),
    url(r'^get-companies-by/', 'myapp.views.getCompaniesByCat'),
    url(r'^get-company-by/', 'myapp.views.getCompanyById'),
    url(r'^get-items-by/', 'myapp.views.getItemsByCompany'),
    url(r'^get-item-cats/', 'myapp.views.getItemCategories'),
    url(r'^make-order/', 'myapp.views.makeOrder'),
    url(r'^add-rating/', 'myapp.views.addrating'),
    url(r'^get-maniac/', 'myapp.views.maniac'),
)
