# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='sort_int',
            field=models.IntegerField(default=0, verbose_name='Поле для сортировки'),
            preserve_default=True,
        ),
    ]
