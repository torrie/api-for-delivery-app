# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0004_rating'),
    ]

    operations = [
        migrations.CreateModel(
            name='Maniac',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('who', models.CharField(verbose_name='Кто сказал', max_length=255)),
                ('what', models.CharField(verbose_name='Что сказал', max_length=255)),
                ('text', models.CharField(verbose_name='Текст кнопки', max_length=255)),
                ('link', models.CharField(verbose_name='Ссылка', max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
