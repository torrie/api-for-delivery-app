# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0003_auto_20150520_1347'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('from_user', models.CharField(max_length=255, verbose_name='От пользователя')),
                ('amount', models.IntegerField(verbose_name='Сколько кинул', default=0)),
                ('to_company', models.ForeignKey(verbose_name='К компании', to='myapp.Company', related_name='rating')),
            ],
            options={
                'verbose_name_plural': 'Оценки',
                'verbose_name': 'Оценка',
            },
            bases=(models.Model,),
        ),
    ]
