# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(verbose_name='Название категории', max_length=255)),
                ('pic_url', models.CharField(verbose_name='Ссылка на картинку', max_length=255)),
            ],
            options={
                'verbose_name': 'Категория',
                'verbose_name_plural': 'Категории',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(verbose_name='Название компании', max_length=255)),
                ('pic_url', models.CharField(verbose_name='Ссылка на картинку', max_length=255)),
                ('logo_url', models.CharField(verbose_name='Ссылка на логотип', max_length=255)),
                ('min_order', models.IntegerField(verbose_name='Минимальная сумма заказа', default=0)),
                ('delivery_price', models.IntegerField(verbose_name='Стоимость доставки', default=0)),
                ('delivery_latency', models.IntegerField(verbose_name='Среднее время ожидания в минутах', default=0)),
                ('contact', models.TextField(verbose_name='Контактная информация', max_length=1000)),
                ('order_time', models.TextField(verbose_name='Время приема заказов', max_length=1000)),
                ('kitchen', models.TextField(verbose_name='Кухня', max_length=1000)),
                ('about', models.TextField(verbose_name='Описание', max_length=1000)),
                ('category', models.ManyToManyField(to='myapp.Category')),
            ],
            options={
                'verbose_name': 'Компания',
                'verbose_name_plural': 'Компании',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(verbose_name='Название блюда', max_length=255)),
                ('pic_url', models.CharField(verbose_name='Ссылка на картинку', max_length=255)),
                ('weight', models.IntegerField(verbose_name='Вес блюда, грамм', default=0)),
                ('new_price', models.IntegerField(verbose_name='Новая цена', default=0)),
                ('old_price', models.IntegerField(verbose_name='Старая цена', default=0)),
                ('about', models.TextField(verbose_name='Описание', max_length=1000)),
                ('visible', models.BooleanField(verbose_name='Видимость в списке', default=True)),
            ],
            options={
                'verbose_name': 'Блюдо',
                'verbose_name_plural': 'Блюда',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(verbose_name='Название категории', max_length=255)),
            ],
            options={
                'verbose_name': 'Категория блюда',
                'verbose_name_plural': 'Категории блюд',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('fio', models.CharField(verbose_name='ФИО', max_length=255)),
                ('telephone', models.CharField(verbose_name='Телефон', max_length=255)),
                ('adres', models.CharField(verbose_name='Адрес', max_length=255)),
                ('other', models.CharField(verbose_name='Другое', max_length=255)),
                ('about', models.TextField(verbose_name='Заказ', max_length=1000)),
                ('summa', models.CharField(verbose_name='Сумма заказа', max_length=255)),
                ('date', models.DateTimeField(verbose_name='Дата заказа', blank=True, default=datetime.datetime.now)),
                ('category', models.ManyToManyField(to='myapp.Item')),
            ],
            options={
                'verbose_name': 'Заказ',
                'verbose_name_plural': 'Заказы',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Town',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(verbose_name='Город', max_length=255)),
            ],
            options={
                'verbose_name': 'Город',
                'verbose_name_plural': 'Города',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='item',
            name='category',
            field=models.ForeignKey(verbose_name='Категория блюда', to='myapp.ItemCategory'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='company',
            field=models.ForeignKey(verbose_name='Компания', to='myapp.Company'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='town',
            field=models.ForeignKey(verbose_name='Город', to='myapp.Town'),
            preserve_default=True,
        ),
    ]
