# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0002_category_sort_int'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='sort_int',
        ),
        migrations.AddField(
            model_name='itemcategory',
            name='sort_int',
            field=models.IntegerField(verbose_name='Поле для сортировки', default=0),
            preserve_default=True,
        ),
    ]
