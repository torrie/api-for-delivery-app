from django.db import models
from datetime import datetime 

# Create your models here.

class Town(models.Model):
    name = models.CharField(max_length=255, verbose_name='Город')

    class Meta:
        verbose_name = "Город"
        verbose_name_plural = "Города"

    def __str__(self):
        return "%s" % self.name


class Category(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название категории')
    pic_url = models.CharField(max_length=255, verbose_name='Ссылка на картинку')

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return "%s" % self.name


class Company(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название компании')
    pic_url = models.CharField(max_length=255, verbose_name='Ссылка на картинку')
    logo_url = models.CharField(max_length=255, verbose_name='Ссылка на логотип')

    min_order = models.IntegerField(default=0, verbose_name='Минимальная сумма заказа')
    delivery_price = models.IntegerField(default=0, verbose_name='Стоимость доставки')
    delivery_latency = models.IntegerField(default=0, verbose_name='Среднее время ожидания в минутах')

    contact = models.TextField(max_length=1000, verbose_name='Контактная информация')
    order_time = models.TextField(max_length=1000, verbose_name='Время приема заказов')
    kitchen = models.TextField(max_length=1000, verbose_name='Кухня')
    about = models.TextField(max_length=1000, verbose_name='Описание')
    
    town = models.ForeignKey(Town, verbose_name='Город')
    category = models.ManyToManyField(Category)
    
    class Meta:
        verbose_name = "Компания"
        verbose_name_plural = "Компании"

    def __str__(self):
        return "%s (%s)" % (self.name, self.town)


class ItemCategory(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название категории')

    sort_int = models.IntegerField(default=0, verbose_name='Поле для сортировки')
    
    class Meta:
        verbose_name = "Категория блюда"
        verbose_name_plural = "Категории блюд"

    def __str__(self):
        return "%s" % self.name


class Item(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название блюда')
    pic_url = models.CharField(max_length=255, verbose_name='Ссылка на картинку')
    weight = models.IntegerField(default=0, verbose_name='Вес блюда, грамм')

    new_price = models.IntegerField(default=0, verbose_name='Новая цена')
    old_price = models.IntegerField(default=0, verbose_name='Старая цена')
    about = models.TextField(max_length=1000, verbose_name='Описание')

    company = models.ForeignKey(Company, verbose_name='Компания')
    category = models.ForeignKey(ItemCategory, verbose_name='Категория блюда')

    visible = models.BooleanField(default=True, verbose_name='Видимость в списке')

    class Meta:
        verbose_name = "Блюдо"
        verbose_name_plural = "Блюда"

    def __str__(self):
        return "%s (%s)" % (self.name, self.company)


class Order(models.Model):
    fio = models.CharField(max_length=255, verbose_name='ФИО')
    telephone = models.CharField(max_length=255, verbose_name='Телефон')
    adres = models.CharField(max_length=255, verbose_name='Адрес')
    other = models.CharField(max_length=255, verbose_name='Другое')

    about = models.TextField(max_length=1000, verbose_name='Заказ')
    summa = models.CharField(max_length=255, verbose_name='Сумма заказа')
    
    category = models.ManyToManyField(Item)

    date = models.DateTimeField(default=datetime.now, blank=True, verbose_name='Дата заказа')
    
    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

    def __str__(self):
        return "%s (%s) на сумму %s" % (self.fio, self.date, self.summa)


class Rating(models.Model):
    from_user = models.CharField(max_length=255, verbose_name='От пользователя')
    to_company = models.ForeignKey(Company, verbose_name='К компании', related_name='rating')
    amount = models.IntegerField(default=0, verbose_name='Сколько кинул')

    
    class Meta:
        verbose_name = "Оценка"
        verbose_name_plural = "Оценки"

    def __str__(self):
        return "%s" % (self.from_user,)

# Приложение Маньяка
class Maniac(models.Model):
    who = models.CharField(max_length=255, verbose_name='Кто сказал')
    what = models.CharField(max_length=255, verbose_name='Что сказал')
    text = models.CharField(max_length=255, verbose_name='Текст кнопки')
    link = models.CharField(max_length=255, verbose_name='Ссылка')