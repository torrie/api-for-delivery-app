from django.contrib import admin
from django.contrib.admin import ModelAdmin
from myapp.models import Town, Category, Company, ItemCategory, Item, Order, Rating, Maniac

# Register your models here.
class TownAdmin(ModelAdmin):
    list_display = ('id', 'name')
admin.site.register(Town, TownAdmin)

class CategoryAdmin(ModelAdmin):
    list_display = ('id', 'name', 'pic_url')
admin.site.register(Category, CategoryAdmin)

class CompanyAdmin(ModelAdmin):
    list_display = ('id', 'name', 'town')
admin.site.register(Company, CompanyAdmin)

class ItemCategoryAdmin(ModelAdmin):
    list_display = ('id', 'name')
admin.site.register(ItemCategory, ItemCategoryAdmin)

class ItemAdmin(ModelAdmin):
    list_display = ('id', 'name', 'company', 'category', 'visible')
admin.site.register(Item, ItemAdmin)

class OrderAdmin(ModelAdmin):
    list_display = ('id', 'fio', 'summa', 'date')
admin.site.register(Order, OrderAdmin)

class RatingAdmin(ModelAdmin):
    list_display = ('id', 'from_user', 'to_company', 'amount')
admin.site.register(Rating, RatingAdmin)

class ManiacAdmin(ModelAdmin):
    list_display = ('who', 'what')
admin.site.register(Maniac, ManiacAdmin)