from django.shortcuts import render
from myapp.models import Town, Category, Company, ItemCategory, Item, Order, Rating, Maniac
from django.http import JsonResponse
from django.core import serializers
import json
from collections import OrderedDict
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def getTowns(request):
    towns = Town.objects.all().values()
    return JsonResponse(list(towns), safe=False)


def getCategories(request):
    # TODO: сделать выборку по компаниям присутствующим
    cats = Category.objects.all().values()
    return JsonResponse(list(cats), safe=False)


def getCompaniesByCat(request):
    if request.method == 'GET':
        category = request.GET.get('category', 0)
        if category == '':
            category = 0
        if int(category)>0:
            companies = Company.objects.filter(town=int(request.GET['town']), category__id=category)
        else:
            companies = Company.objects.filter(town=int(request.GET['town']))
        comps = []
        user_id = int(request.GET.get('user_id', 0))
        for comp in companies:
            
            if comp.rating.filter(from_user=user_id).count() > 0:
                comps.append({
                    "name":comp.name,
                    "order_time":comp.order_time,
                    "delivery_price":comp.delivery_price,
                    "min_order":comp.min_order,
                    "about":comp.about,
                    "town_id":comp.town_id,
                    "kitchen":comp.kitchen,
                    "id":comp.id,
                    "contact":comp.contact,
                    "delivery_latency":comp.delivery_latency,
                    "logo_url":comp.logo_url,
                    "pic_url":comp.pic_url,
                    "rating":comp.rating.count(),
                    "voted":1
                    })
            else:
                comps.append({
                    "name":comp.name,
                    "order_time":comp.order_time,
                    "delivery_price":comp.delivery_price,
                    "min_order":comp.min_order,
                    "about":comp.about,
                    "town_id":comp.town_id,
                    "kitchen":comp.kitchen,
                    "id":comp.id,
                    "contact":comp.contact,
                    "delivery_latency":comp.delivery_latency,
                    "logo_url":comp.logo_url,
                    "pic_url":comp.pic_url,
                    "rating":comp.rating.count(),
                    "voted":0
                    })
        if int(category)>0:
            companies = {
                'title': Category.objects.get(id=int(category)).name, 
                'list': comps,
            }
        else:
            companies = {
                'title': 'Все компании', 
                'list': comps,
            }     

        return JsonResponse(companies, safe=False)
    else:
        return JsonResponse({'result':'error', 'error':'ony GET'})


def getCompanyById(request):
    if request.method == 'GET':
        comp = Company.objects.get(id=int(request.GET['id']))
        rates = comp.rating.all()
        user_id = int(request.GET.get('user_id', 0))
        
        if comp.rating.filter(from_user=user_id).count() > 0:
            comps = {
                "name":comp.name,
                "order_time":comp.order_time,
                "delivery_price":comp.delivery_price,
                "min_order":comp.min_order,
                "about":comp.about,
                "town_id":comp.town_id,
                "kitchen":comp.kitchen,
                "id":comp.id,
                "contact":comp.contact,
                "delivery_latency":comp.delivery_latency,
                "logo_url":comp.logo_url,
                "pic_url":comp.pic_url,
                "rating":comp.rating.count(),
                "voted":1
                }
        else:
            comps = {
                "name":comp.name,
                "order_time":comp.order_time,
                "delivery_price":comp.delivery_price,
                "min_order":comp.min_order,
                "about":comp.about,
                "town_id":comp.town_id,
                "kitchen":comp.kitchen,
                "id":comp.id,
                "contact":comp.contact,
                "delivery_latency":comp.delivery_latency,
                "logo_url":comp.logo_url,
                "pic_url":comp.pic_url,
                "rating":comp.rating.count(),
                "voted":0
                }
        return JsonResponse(comps, safe=False)
    else:
        return JsonResponse({'result':'error', 'error':'ony GET'})


def getItemsByCompany(request):
    if request.method == 'GET':
        items = Item.objects.filter(company=int(request.GET['company']), visible=True)
        res = []
        
        categories = []
        for item in items:
            if not item.category.name in categories:
                categories.append(item.category.name)


        mycats = ItemCategory.objects.filter(name__in=categories).order_by('-sort_int')
        categories = []
        for mycat in mycats:
            categories.append(mycat.name)

        ids = []
        for mycat in mycats:
            ids.append(mycat.sort_int)
        # у нас есть список всех id категорий
        for cat in categories:
            items2 = items.filter(category__name=cat)
            nn = []
            for item2 in items2:
                nn.append(
                    {
                        'id': item2.id,
                        'name': item2.name,
                        'pic_url': item2.pic_url,
                        'weight': item2.weight,

                        'new_price': item2.new_price,
                        'old_price': item2.old_price,
                        'about': item2.about,
                    }
                    )
            res.append({
                    'name': cat,
                    'sort': ids[categories.index(cat)],
                    'list': nn
                    })



        # сортируем по числу коммитов в обратном порядке
        return HttpResponse(json.dumps(res))
    else:
        return JsonResponse({'result':'error', 'error':'ony GET'})


def getItemCategories(request):
    # TODO: by company id
    categories = ItemCategory.objects.all()
    return JsonResponse(list(categories.values()), safe=False)


# def getCompanyList(request):
#     if request.method == 'GET':
#         items = Item.objects.filter(company=int(request.GET['company']))
#         categories=[]
#         for item in items:
#             categories.extend(item.category.all())
#         for category in categories:
#             listy = []
#             my = items.filter(category__contains=)

#         return JsonResponse({})
#     else:
#         return JsonResponse({'result':'error', 'error':'ony GET'})

@csrf_exempt
def makeOrder(request):
    if request.method == 'POST':
        order = Order(fio=request.POST['fio'], telephone=request.POST['phone'], adres=request.POST['adres'], other=request.POST['other'], about=request.POST['zakaz'], summa=request.POST['summa'])
        order.save()
        return JsonResponse({'result':'ok', 'order':order.id})
    else:
        return JsonResponse({'result':'error', 'error':'ony POST'})


def addrating(request):
    if (request.method == 'GET'):
        try:
            user_id = int(request.GET['user_id'])
            to_company = int(request.GET['to_company'])

            ct = Company.objects.get(id=to_company)

            # Если пользователь уже голосовал за это выполненное задание - возвращаем отмену.
            count_user_votes = Rating.objects.filter(from_user=user_id, to_company=ct)
            if (count_user_votes.count() > 0):
                count_user_votes[0].delete()
                return JsonResponse({'success': True})

            rct = Rating(from_user=user_id, to_company=ct, amount=1)
            rct.save()

            return JsonResponse({'success': True})
        except:
            return JsonResponse({'success': False, 'error': 'some error'})
    return JsonResponse({'success': False})


def maniac(request):
    if request.method == 'GET':
        rand = Maniac.objects.all().order_by('?')[0]
        return JsonResponse(
            {
            'who':rand.who, 
            'what':rand.what,
            'text':rand.text,
            'link':rand.link,
            }
            )
    else:
        return JsonResponse({'result':'error', 'error':'ony GET'})
